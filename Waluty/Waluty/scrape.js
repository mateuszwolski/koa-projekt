

/**
 * Created by mateusz on 21.03.15.
 */

var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var usage = require('usage');
var co = require('co');

url = 'http://kursy-walut.mybank.pl/';
function scrape(){
    var jsonArray = [];
    request(url, function(error, response, html) {
        if (!error) {
            console.log('brak errorow');
            var $ = cheerio.load(html);

            var currency,value,change,flow;

            var i =0;

            $('.box_mini').filter(function () {

                var data = $(this);

                var json = new Object();


                currency = data.children().first().text();
                value = data.children().first().next().text();
                change = data.children().first().next().next().text();
                if (data.children().first().next().next().hasClass('b3 up2')) {

                flow ='up';
                    }
                else
                    {
                        flow = 'down';
                    }

                json.currency = currency;
                json.value = value;
                json.change = change;
                json.flow = flow;

                jsonArray[i]= json;

                i++;
            })

           fs.appendFile('message.txt', '\n'+new Date()+'\n'+JSON.stringify(jsonArray), function (err) {
               if (err) throw err;
           });

           fs.writeFile('message22.txt',JSON.stringify(jsonArray), function (err) {
               if (err) throw err;
           });

        }
    })
}

exports.scrape = scrape;
