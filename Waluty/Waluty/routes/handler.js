'use strict';
var parse = require('co-body');
var co = require('co');
var fs = require('co-fs');



module.exports.rate = function * rate(currency,next) {
  if ('GET' != this.method) return yield next;

  var rates = yield fs.readFile('message22.txt');

  var currencies = JSON.parse(rates);

  for(var i=0;i<currencies.length;i++){
    if(currency === currencies[i].currency){
      this.body = currencies[i];
    }
  }
};

module.exports.rates = function * rates(next) {
  if ('GET' != this.method) return yield next;

  var rates = yield fs.readFile('message22.txt');

  this.body = JSON.parse(rates);
  
};
