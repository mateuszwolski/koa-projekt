var scrape = require("./scrape");
var CronJob = require('cron').CronJob;

function work(){
  scrape.scrape();
var job = new CronJob('0 */5 * * * *', function(){
    scrape.scrape();
    console.log(new Date());
    }, function () {
        console.log('job stopped');
      },
      false,
      "Poland"
);

job.start();

}
exports.work = work;
