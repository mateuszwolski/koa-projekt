
var koa = require('koa');
var route = require('koa-route');

var rates = require('./routes/rates');
var rate = require('./routes/rate');

var scrapeTasker = require("./scrapeTasker");

app.use(route.get('/rates', rates));
app.use(route.get('/currencyRate', rate));

app.listen(8008);
console.log('Koa listening on port 8008');

scrapeTasker.work();
