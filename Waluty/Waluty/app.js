'use strict';
var handler = require('./routes/handler');
//var compress = require('koa-compress');
var logger = require('koa-logger');
var route = require('koa-route');
var koa = require('koa');
var path = require('path');
var app = module.exports = koa();
var scrapeTasker = require("./scrapeTasker");
// Logger
app.use(logger());

app.use(route.get('/rates', handler.rates));
app.use(route.get('/currencyRate/:currency', handler.rate));

// Compress
//app.use(compress());

if (!module.parent) {
  app.listen(1337);
  console.log('listening on port 1337');
}
scrapeTasker.work();
