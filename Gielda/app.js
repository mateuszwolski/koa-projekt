'use strict';
var stockChangeTasker = require('./stockChangeTasker');
var handler = require('./handler');
//var compress = require('koa-compress');
var logger = require('koa-logger');
var route = require('koa-route');
var koa = require('koa');
var path = require('path');
var koaBody   = require('koa-body');
var app = module.exports = koa();
app.use(koaBody());
require('koa-qs')(app)

app.use(route.get('/stocks', handler.stocks));
app.use(route.get('/stock', handler.stock));
app.use(route.post('/stock/sell', handler.sell));
app.use(route.post('/stock/buy', handler.buy));
// Compress
//app.use(compress());

if (!module.parent) {
  app.listen(1339);
  console.log('listening on port 1339');
}


// server.get('/rates', rates);
// server.get('/currencyRate', rate);

stockChangeTasker.work();
