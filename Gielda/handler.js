/**
 * Created by mateusz on 14.03.15.
 */
var querystring = require("querystring");
var readActualStocks = require('./readActualStocks');
var currencyRate = require("./currencyRate");
var calculate = require('./calculate');
var model = require('./model');
var validator = require('./validator');
var parse = require('co-body');
var co = require('co');
var fs = require('co-fs');
var ClientModel = model.ClientProvider;
var TransactionModel = model.TransactionProvider;
var ClientProvider= new ClientModel();
var TransactionProvider= new TransactionModel();

  module.exports.stocks = function * stocks(next) {
    if ('GET' != this.method) return yield next;
    var currency = this.query.currency;

    var data = yield fs.readFile('actualStocks.txt');
    var dat = JSON.parse(data);

    var rate = yield currencyRate.getCurrencyRate(currency);
    rate = JSON.parse(rate);

    for(var i=0;i<dat.length;i++){
            dat[i].value = calculate.calculate(dat[i].value,rate.value);
    }
    this.body = dat;
  };


  module.exports.stock = function * stock(next) {
    if ('GET' != this.method) return yield next;
    var stock = this.query.stock;
    var currency = this.query.currency;
    yield readFile(stock,currency,this);
  };

  module.exports.sell = function * sell(data,next) {
    if ('POST' != this.method) return yield next;

    yield sellStock(this,this.request.body);
  };

  module.exports.buy = function * buy(data,next) {
    if ('POST' != this.method) return yield next;

    yield buyStock(this,this.request.body);
  };

  //
  // app.post('/stock/sell', function(req, res, next) {
  //   var body = req.body;
  //   sellStock(res,body);
  // });
  //
  // app.post('/stock/buy', function(req, res, next) {
  //   var body = req.body;
  //   buyStock(res,body);
  // });


  // function sellStock(response,data) {
  //   console.log('x');
  //     //var data = JSON.parse(postData);
  //     console.log('x');
  //     checkThatStockIsCorrect(data.stockId,function(is){
  //       console.log('x');
  //       if(!is){
  //         responseBadRequest(response,"Stock not exists");
  //         return;
  //       }
  //       ClientProvider.findByLogin(data.login,function(error, results){
  //         if(error) throw error;
  //
  //         if(results ===null){
  //           responseBadRequest(response,"User not exist");
  //           return;
  //         }
  //         var stocks = results.stocks;
  //
  //         var changeResult = tryRemoveStocks(stocks,data.stockId,data.stocksCount);
  //         if(changeResult === -1){
  //           responseBadRequest(response,"You don't have enaught stocks");
  //           return;
  //         }
  //         if(changeResult === 0){
  //           responseBadRequest(response,"You don't have "+data.stockId+" stock");
  //           return;
  //         }
  //         //update users actions count
  //         ClientProvider.updateById(results._id, changeResult, function(error, result){
  //           if(error) throw error;
  //         });
  //         //get currency
  //         currencyRate.getCurrencyRate(data.currency,function(error,opd){
  //           if(error) throw error;
  //           findStockCost(data.stockId,function(value){
  //             var rate  = opd.value.replace(",", ".");
  //             var oneStockAfterExchange = calculate.calculate(value,opd.value);
  //             var stocksAmount = oneStockAfterExchange * data.stocksCount;
  //             stocksAmount =  Math.round((parseFloat(stocksAmount)*100))/100;
  //             TransactionProvider.save(data.login,'sell',data.stockId,data.stocksCount,data.currency,rate,stocksAmount,function(error,result){
  //               if(error) throw error;
  //
  //               response.status(200);
  //               response.send('zarobilem'+stocksAmount);
  //
  //             });
  //           });
  //         });
  //
  //       });
  //   });
  //     //validator.sellValidator(postData);
  // }
  //

  function * sellStock(response,data) {

      var is = yield checkThatStockIsCorrect(data.stockId);

        if(!is){
          response.body = "Stock not exists";
          return;
        }

        var results = yield ClientProvider.findByLogin(data.login);

          if(results ===null){
            response.body = "User not exist";
            return;
          }
          var stocks = results.stocks;

          var changeResult = tryRemoveStocks(stocks,data.stockId,data.stocksCount);

          if(changeResult === -1){
            response.body = "You don't have enaught stocks";
            return;
          }
          if(changeResult === 0){
            response.body = "You don't have "+data.stockId+" stock";
            return;
          }
          //update users actions count
          var result = yield ClientProvider.updateById(results._id, changeResult);

          //get currency
          var opd = yield  currencyRate.getCurrencyRate(data.currency);
          opd = JSON.parse(opd);
          var value = yield findStockCost(data.stockId);
              var rate  = opd.value.replace(",", ".");
              var oneStockAfterExchange = calculate.calculate(value,opd.value);
              var stocksAmount = oneStockAfterExchange * data.stocksCount;
              stocksAmount =  Math.round((parseFloat(stocksAmount)*100))/100;
              yield TransactionProvider.save(data.login,'sell',data.stockId,data.stocksCount,data.currency,rate,stocksAmount);

              response.body = 'zarobilem'+stocksAmount;
  }

  function * buyStock(response,data) {

    var checkStatus = yield checkThatStockIsCorrect(data.stockId);

      if(!checkStatus){
        response.body = "Stock not exists";
        return;
      }

      var results = yield ClientProvider.findByLogin(data.login);
      if(results === null){
        response.body = "Stock not exists";
        return;
        }
          var client = addStockToClient(results,data.stockId,data.stocksCount);
          yield ClientProvider.updateById(results._id,client.stocks,function(){});
          var opd = yield currencyRate.getCurrencyRate(data.currency);
          opd = JSON.parse(opd);
          var value = yield findStockCost(data.stockId);
              var rate  = opd.value.replace(",", ".");
              var oneStockAfterExchange = calculate.calculate(value,opd.value);
              var stocksAmount = oneStockAfterExchange * data.stocksCount;
              stocksAmount =  Math.round((parseFloat(stocksAmount)*100))/100;
              yield  TransactionProvider.save(data.login,'buy',data.stockId,data.stocksCount,data.currency,rate,stocksAmount)
                response.body = 'Wydałes'+stocksAmount;
  }
  //






  // function buyStock(response,data) {
  //   //var data = JSON.parse(postData);
  //   checkThatStockIsCorrect(data.stockId,function(is){
  //     if(!is){
  //       responseBadRequest(response,"Stock not exists");
  //       return;
  //     }
  //     ClientProvider.findByLogin(data.login,function(error, results){
  //       if(error) throw error;
  //
  //       if(results === null){
  //         responseBadRequest(response,"User not exist");
  //         return;
  //       }
  //       var client = addStockToClient(results,data.stockId,data.stocksCount);
  //
  //       ClientProvider.updateById(results._id,client.stocks,function(){});
  //
  //       //get currency
  //       currencyRate.getCurrencyRate(data.currency,function(error,opd){
  //         if(error) throw error;
  //         findStockCost(data.stockId,function(value){
  //           var rate  = opd.value.replace(",", ".");
  //           var oneStockAfterExchange = calculate.calculate(value,opd.value);
  //           var stocksAmount = oneStockAfterExchange * data.stocksCount;
  //           stocksAmount =  Math.round((parseFloat(stocksAmount)*100))/100;
  //           TransactionProvider.save(data.login,'buy',data.stockId,data.stocksCount,data.currency,rate,stocksAmount,function(error,result){
  //             if(error) throw error;
  //
  //             response.status(200);
  //             response.send('Wydałes'+stocksAmount);
  //
  //           });
  //         });
  //       });
  //
  //
  //     });
  //   });
  // }
  //
  //
  //
  function * readFile(stock,currency,res){
    var data = yield fs.readFile('actualStocks.txt');
          var stocks = JSON.parse(data);
            for(var i=0;i<stocks.length;i++){
              if(stock === stocks[i].id){
                var x = stocks[i];
                var rate = yield currencyRate.getCurrencyRate(currency);
                rate = JSON.parse(rate);
                  x.value = calculate.calculate(x.value,rate.value);
                  res.body = x;
                };
            }
      }

  // function responseBadRequest(body,message){
  //   body = message;
  //   return;
  // }
  //
  function findClientStock(stocks,stock){
    for(var i=0;i<stocks.length;i++){
      if(stocks[i].stockId === stock){
        return stocks[i];
      }
    }
  }
  // //-1 not match stocks
  // //0 not found
  function tryRemoveStocks(stocks,stock,changeValue){
    for(var i=0;i<stocks.length;i++){
      if(stocks[i].stockId === stock){
        if(changeValue>stocks[i].count){
          return -1;
        }
        stocks[i].count = stocks[i].count - changeValue;
        return stocks;
      }
    }
    return 0;
  }
  //
  // function findStockCost(stock,callback){
  //   readActualStocks.readActualStocks('actualStocks.txt',function(error,data){
  //     if(error) throw error
  //       var stocks = JSON.parse(data);
  //         for(var i=0;i<stocks.length;i++){
  //           if(stock === stocks[i].id){
  //             var x = stocks[i].value;
  //             callback(x);
  //           }
  //         }
  //
  //   });
  // }


  function * findStockCost(stock){
    var data = yield fs.readFile('actualStocks.txt');

        var stocks = JSON.parse(data);
          for(var i=0;i<stocks.length;i++){
            if(stock === stocks[i].id){
              var x = stocks[i].value;
              return x;
            }
          }
  }


  //
  function addStockToClient(client,stockId,stockCount){
    var clientStock = client.stocks;
    for(var i=0;i<clientStock.length;i++){
      if(clientStock[i].stockId === stockId){
          clientStock[i].count = clientStock[i].count + stockCount;
          client.stocks = clientStock;
          return client;
      }
    }
    var newStock = { stockId: stockId, count: stockCount};
    clientStock[clientStock.length] = newStock;
    client.stocks = clientStock;
    return client;
  }
  //
  //
  function * checkThatStockIsCorrect(stockId){
    var data = yield fs.readFile('actualStocks.txt');
    console.log(stockId);

        var stocks = JSON.parse(data);
          for(var i=0;i<stocks.length;i++){
            if(stockId === stocks[i].id){
              return true;
            }
          }
          return false;
  }
