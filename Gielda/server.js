/**
 * Created by mateusz on 14.03.15.
 */
'use strict';
var http = require("http");
var url = require("url");
var httpPort = 12222;
function start(route,handle) {
    function onRequest(request, response) {
        var postData = "";
        var pathname = url.parse(request.url).pathname;
        var query = url.parse(request.url).query;
        //var param = request.param("currency");
        var method = request.method.toLowerCase();
        console.log("Request for " + pathname + " received.Method: "+method+" query:"+query);

        request.setEncoding("utf8");

        request.addListener("data", function(postDataChunk) {
           postData += postDataChunk;
           console.log("Received POST data chunk '"+
           postDataChunk + "'.");
         });
         request.addListener("end", function() {
           console.log('posztem poszlo:'+postData);
           route(handle, pathname, response,query,postData);
         });

        // route(handle, pathname, response,query,postData);

    }
    http.createServer(onRequest).listen(httpPort);
    console.log("Server for currency rate has started: "+httpPort);
}
exports.start = start;
