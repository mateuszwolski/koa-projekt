/**
 * Created by mateusz on 14.03.15.
 */
'use strict';
var querystring = require("querystring");
var fs = require('fs');
var readActualStocks = require('./readActualStocks');
var currencyRate = require("./currencyRate");
var calculate = require('./calculate');
var model = require('./model');
var validator = require('./validator');
var ClientModel = model.ClientProvider;
var TransactionModel = model.TransactionProvider;
var ClientProvider= new ClientModel();
var TransactionProvider= new TransactionModel();

function getAllStocksRate(response,query,postData) {

    var stock =  query.split("=");

    readActualStocks.readActualStocks('actualStocks.txt',function(error,data){
      currencyRate.getCurrencyRate(stock[1],function(opd){
        var dat = JSON.parse(data);
        for(var i=0;i<dat.length;i++){
          dat[i].value = calculate.calculate(dat[i].value,opd.value);
        }
        response.writeHead(200, {"Content-Type": "application/json"});
        response.write(JSON.stringify(dat));
        response.end();
      });
    });
}

function getStockRate(response,query,postData) {
    readFile(query,response);
}


function readFile(option,response){
  var pathParams =  option.split("&");
  if(pathParams.length==2){
    var stock =  pathParams[0].split("=");
    var currency =  pathParams[1].split("=");
    readActualStocks.readActualStocks('actualStocks.txt',function(error,data){

        var stocks = JSON.parse(data);
          for(var i=0;i<stocks.length;i++){
            if(stock[1] === stocks[i].id){
              var x = stocks[i];
              currencyRate.getCurrencyRate(currency[1], function(opd){
                x.value = calculate.calculate(x.value,opd.value);
                response.writeHead(200, {"Content-Type": "application/json"});
                response.write(""+JSON.stringify(x));
                response.end();
              });

            }
          }

    });
  }else{
    responseBadRequest(response);
  }
}
function responseBadRequest(response,message){
  response.writeHead(400, {"Content-Type": "text/plain"});
  response.write(message);
  response.end();
}

function sellStock(response,query,postData) {
    var data = JSON.parse(postData);
    checkThatStockIsCorrect(data.stockId,function(is){
      if(!is){
        responseBadRequest(response,"Stock not exists");
        return;
      }
      ClientProvider.findByLogin(data.login,function(error, results){
        if(error) throw error;

        if(results ===null){
          responseBadRequest(response,"User not exist");
          return;
        }
        var stocks = results.stocks;

        var changeResult = tryRemoveStocks(stocks,data.stockId,data.stocksCount);
        if(changeResult === -1){
          responseBadRequest(response,"You don't have enaught stocks");
          return;
        }
        if(changeResult === 0){
          responseBadRequest(response,"You don't have "+data.stockId+" stock");
          return;
        }
        //update users actions count
        ClientProvider.updateById(results._id, changeResult, function(error, result){
          if(error) throw error;
        });
        //get currency
        currencyRate.getCurrencyRate(data.currency,function(opd){
          findStockCost(data.stockId,function(value){
            var rate  = opd.value.replace(",", ".");
            var oneStockAfterExchange = calculate.calculate(value,opd.value);
            var stocksAmount = oneStockAfterExchange * data.stocksCount;
            TransactionProvider.save(data.login,'sell',data.stockId,data.stocksCount,data.currency,rate,stocksAmount,function(error,result){
              if(error) throw error;

              response.writeHead(200, {"Content-Type": "text/plain"});
              response.write('zarobilem'+stocksAmount);
              response.end();

            });
          });
        });

      });
  });
    //validator.sellValidator(postData);
}

function buyStock(response,query,postData) {
  var data = JSON.parse(postData);
  checkThatStockIsCorrect(data.stockId,function(is){
    if(!is){
      responseBadRequest(response,"Stock not exists");
      return;
    }
    ClientProvider.findByLogin(data.login,function(error, results){
      if(error) throw error;

      if(results === null){
        responseBadRequest(response,"User not exist");
        return;
      }
      var client = addStockToClient(results,data.stockId,data.stocksCount);

      ClientProvider.updateById(results._id,client.stocks,function(){});

      //get currency
      currencyRate.getCurrencyRate(data.currency,function(opd){
        findStockCost(data.stockId,function(value){
          var rate  = opd.value.replace(",", ".");
          var oneStockAfterExchange = calculate.calculate(value,opd.value);
          var stocksAmount = oneStockAfterExchange * data.stocksCount;
          TransactionProvider.save(data.login,'buy',data.stockId,data.stocksCount,data.currency,rate,stocksAmount,function(error,result){
            if(error) throw error;

            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write('Wywałeś'+stocksAmount);
            response.end();

          });
        });
      });


    });
  });
}
function findClientStock(stocks,stock){
  for(var i=0;i<stocks.length;i++){
    if(stocks[i].stockId === stock){
      return stocks[i];
    }
  }
}
//-1 not match stocks
//0 not found
function tryRemoveStocks(stocks,stock,changeValue){
  for(var i=0;i<stocks.length;i++){
    if(stocks[i].stockId === stock){
      if(changeValue>stocks[i].count){
        return -1;
      }
      stocks[i].count = stocks[i].count - changeValue;
      return stocks;
    }
  }
  return 0;
}

function findStockCost(stock,callback){
  readActualStocks.readActualStocks('actualStocks.txt',function(error,data){
    if(error) throw error
      var stocks = JSON.parse(data);
        for(var i=0;i<stocks.length;i++){
          if(stock === stocks[i].id){
            var x = stocks[i].value;
            callback(x);
          }
        }

  });
}

function addStockToClient(client,stockId,stockCount){
  var clientStock = client.stocks;
  for(var i=0;i<clientStock.length;i++){
    if(clientStock[i].stockId === stockId){
        clientStock[i].count = clientStock[i].count + stockCount;
        client.stocks = clientStock;
        return client;
    }
  }
  var newStock = { stockId: stockId, count: stockCount};
  clientStock[clientStock.length] = newStock;
  client.stocks = clientStock;
  return client;
}


function checkThatStockIsCorrect(stockId, callback){
  readActualStocks.readActualStocks('actualStocks.txt',function(error,data){
    if(error) throw error
      var stocks = JSON.parse(data);
        for(var i=0;i<stocks.length;i++){
          if(stockId === stocks[i].id){
            callback(true);
            return;
          }
        }
        callback(false);

  });
}

// var array = [];
// console.log('zobaczymy');
// var x = {stockId: "ACP", count: 550};
// var y = {stockId: "MBK", count: 850};
// array[0] = x;
// array[1] = y;
// ClientProvider.save("dupa3",array,function(error, results){
//   if(error) throw error;
//
//
//   console.log('dziala?');
// });

//
TransactionProvider.findAll(function(error, results){
    if(error) throw error;

    console.log(results);
});
//
ClientProvider.findAll(function(error, results){
    if(error) throw error;

    console.log(results);
});
//
exports.getAllStocksRate = getAllStocksRate;
exports.getStockRate = getStockRate;
exports.sellStock = sellStock;
exports.buyStock = buyStock;
