var mongoose = require('mongoose');
var co = require('co');
mongoose.connect('mongodb://localhost/gielda');

var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;



  var Client = new Schema({
      id         : ObjectId,
      login      : { type: String, unique: true },
      stocks     : [{ stockId: String, count: Number }]
  });

  var Transaction = new Schema({
      id            : ObjectId,
      date          : String,
      client        : String,
      operation     : String,
      stockId       : String,
      stocksCount   : Number,
      clientCurrency: String,
      currencyRate  : Number,
      amount        : Number
  });

  mongoose.model('Client', Client);
  mongoose.model('Transaction', Transaction);

  var Client = mongoose.model('Client');
  var Transaction = mongoose.model('Transaction');

  ClientProvider = function(){};
  TransactionProvider = function(){};

  //Find all clients
  ClientProvider.prototype.findAll = function * () {
    var results = yield Client.find({});
    return results;
  };
  //
  // //Find all transactions
  TransactionProvider.prototype.findAll = function * () {
    var results = yield Transaction.find({});
    return results;
  };
  //
  //
  // //Find client by ID
  ClientProvider.prototype.findById = function * (id) {
    var results = yield Client.findById(id);
    return results;

  };
  //
  // //Find Transaction by ID
  TransactionProvider.prototype.findById = function * (id) {
    var results = yield Transaction.findById(id);
    return results;
  };
  //
  // //Find client by login
  ClientProvider.prototype.findByLogin = function * (login) {
    var results = yield Client.findOne({login: login});
    return results;
  };
  //
  //
  //   //Update client by ID
  ClientProvider.prototype.updateById = function * (id, body) {
    console.log(id);
    var client = yield Client.findById(id);
    client.stocks = body;
    yield client.save();
    return;
  };
  //
  //
  // //Create a client
  ClientProvider.prototype.save = function * (loginVal,stocksVal) {
    var client = new Client({login: loginVal, stocks: stocksVal});
    yield client.save();
    return;
  };
  //
  // //Create a transaction
  TransactionProvider.prototype.save = function * (client,operation,stockId,stocksCount,clientCurrency,currencyRate,amount) {
    var transaction = new Transaction({date: new Date(), client: client,operation: operation,stockId: stockId,
    stocksCount: stocksCount,clientCurrency: clientCurrency,currencyRate: currencyRate,
    amount: amount });

    yield transaction.save();
    return;
  };

  exports.ClientProvider = ClientProvider;
  exports.TransactionProvider = TransactionProvider;
