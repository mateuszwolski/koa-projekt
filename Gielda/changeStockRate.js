var fs = require('fs');
var readActualStocks = require('./readActualStocks');

function changeStockRate(){

  readActualStocks.readActualStocks('actualStocks.txt',function(error,data){

    fs.appendFile('stocks.txt', '\n'+new Date()+'\n'+JSON.stringify(JSON.parse(data)), function (err) {
        if (err) throw err;
    });

    var stocks = JSON.parse(data);
    for(var i=0;i<stocks.length;i++){
      stocks[i].value = (parseFloat(stocks[i].value) * parseFloat(randomChange()))/100 + parseFloat(stocks[i].value);
      stocks[i].value = Math.round(stocks[i].value * 100)/100;
    }


    fs.writeFile('actualStocks.txt',JSON.stringify(stocks), function (err) {
        if (err) throw err;
    });

  });
}

function randomChange(){
  return Math.round((Math.random() * (3 - (-3)) + (-3)) * 100)/100;
}
exports.changeStockRate = changeStockRate;
