
'use strict';
var request = require('koa-request');

var url = 'http://127.0.0.1:1337/currencyRate/';

exports.getCurrencyRate = function * getCurrencyRate(currency){

  var options = {
        url: url+currency,
        headers: { 'User-Agent': 'request' }
    };

  var response = yield request(options);
  return response.body;
}
