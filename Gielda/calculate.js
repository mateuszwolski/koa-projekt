
function calculate(stockCost,currencyRate){
  currencyRate = currencyRate.replace(",", "."); 
  return Math.round((parseFloat(stockCost)/parseFloat(currencyRate))*100)/100;
}

exports.calculate = calculate;
